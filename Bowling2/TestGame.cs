﻿namespace Bowling2
{
    using NUnit.Framework;

    [TestFixture]
    public class TestGame
    {
        private Game _game;

        [SetUp]
        public void Setup()
        {
            _game = new Game();
        }

        [Test]
        public void NewGame_OneThrowOnePinOnePoint()
        {
            _game.Throws(1);
            Assert.AreEqual(1, _game.Score);
        }

        [Test]
        public void NewGame_CanThrowAtLeast20TimesWithNoStrike()
        {
            for (int i = 0; i < 20; i++)
            {
                _game.Throws(1);
            }

            Assert.AreEqual(20, _game.Score);
            Assert.That(_game.HasEnded, Is.True);
        }

        [Test]
        public void NewGame_OneStrikeThen1Pin18Times_ScoreIs10Plus2Plus18()
        {
            _game.Throws(10);
            for (int i = 0; i < 18; i++)
            {
                _game.Throws(1);
            }

            Assert.AreEqual(30, _game.Score);
            Assert.IsTrue(_game.HasEnded);
        }

        [Test]
        public void NewGame_OneSpareThen1Pin18Times_ScoreIs10Plus1Plus18()
        {
            _game.Throws(5);
            _game.Throws(5);
            for (int i = 0; i < 18; i++)
            {
                _game.Throws(1);
            }

            Assert.AreEqual(29, _game.Score);
            Assert.IsTrue(_game.HasEnded);
        }

        [Test]
        public void NewGame_StrikeAtLastFrame_AddTwoThrows()
        {
            for (int i = 0; i < 18; i++)
            {
                _game.Throws(1);
            }
            _game.Throws(10);

            Assert.AreEqual(18 + 10, _game.Score);
            Assert.IsFalse(_game.HasEnded);
        }

        [Test]
        public void NewGame_SpareAtLastFrame_AddOneThrow()
        {
            for (int i = 0; i < 18; i++)
            {
                _game.Throws(1);
            }
            _game.Throws(5);
            _game.Throws(5);

            Assert.AreEqual(18 + 10, _game.Score);
            Assert.IsFalse(_game.HasEnded);
        }
        
        [Test]
        public void NewGame_SpareThenOneThrow_Ended()
        {
            for (int i = 0; i < 18; i++)
            {
                _game.Throws(1);
            }
            _game.Throws(5);
            _game.Throws(5);
            _game.Throws(5);

            Assert.AreEqual(18 + 10 + 10, _game.Score);
            Assert.IsTrue(_game.HasEnded);
        }

        [Test]
        public void NewGame_StrikeThenTwoThrows_Ended()
        {
            for (int i = 0; i < 18; i++)
            {
                _game.Throws(1);
            }
            _game.Throws(10);
            _game.Throws(5);
            _game.Throws(5);

            Assert.AreEqual(18 + 10 + 10 + 10, _game.Score);
            Assert.IsTrue(_game.HasEnded);
        }

        [Test]
        public void NewGame_PerfectGame_ScoreIs300()
        {
            for (int i = 0; i < 10; i++)
            {
                _game.Throws(10);
            }

            _game.Throws(10);
            _game.Throws(10);

            Assert.AreEqual(300, _game.Score);
            Assert.IsTrue(_game.HasEnded);
        }

        [Test]
        public void NewGame_TwoStrikesThen1_ScoreIs22Plus12Plus16()
        {
            _game.Throws(10);
            _game.Throws(10);
            for (int i = 0; i < 8; i++)
            {
                _game.Throws(1);
                _game.Throws(1);
            }

            Assert.AreEqual(50, _game.Score);
            Assert.IsTrue(_game.HasEnded);
        }
    }
}
