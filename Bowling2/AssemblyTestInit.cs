﻿/*
 * This file is owned by TalentSoft.NUnit.Setup NuGet package and should not be modified.
 */

// ReSharper disable CheckNamespace
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

/// <summary>
/// Class that provide tests set-up at assembly level.
/// </summary>
[SetUpFixture]
[SuppressMessage("TalentSoft.StyleCopRules.TSRules", "TS0601:ClassShouldBeIncludedInANamespace", Justification = "This is a set up class for the whole assembly and must have no namespace")]
public partial class AssemblyTestInit
{
    // do not add code here. This file is the root declaration of the partial and must be used only to define documentation and single usage attributes
}