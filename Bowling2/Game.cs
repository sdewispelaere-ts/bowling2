﻿namespace Bowling2
{
    using NUnit.Framework.Constraints;

    internal class Game
    {
        private const int NUMBER_OF_FRAME_IN_GAME = 10;

        private int _numberOfThrowsInFrame;

        private int _numberOfFrames;

        private int _pinDownInFrame;

        private int _numberOfBonusPoints;

        private int _numberOfStrikes;

        private bool IsLastFrame => _numberOfFrames == NUMBER_OF_FRAME_IN_GAME - 1;

        public bool HasEnded => _numberOfFrames == NUMBER_OF_FRAME_IN_GAME;

        public int Score { get; set; }

        public void Throws(int i)
        {
            AddBonusPoints(i);
            _pinDownInFrame += i;

            CheckIfSpareOrStrike();

            Score += i;
            _numberOfThrowsInFrame++;

            if (ShouldMoveToNextFrame())
            {
                MoveToNextFrame();
            }
            else
            {
                if (_pinDownInFrame == 10)
                {
                    _pinDownInFrame = 0;
                }
            }
        }

        private bool ShouldMoveToNextFrame()
        {
            bool isLastFrameWithStrike = _numberOfBonusPoints > 0 && IsLastFrame;
            bool isLastFrameWithSpare = _numberOfBonusPoints > 0 && IsLastFrame;

            return (_numberOfThrowsInFrame >= 2 || _pinDownInFrame == 10) 
                && !isLastFrameWithStrike && !isLastFrameWithSpare;
        }

        private void MoveToNextFrame()
        {
            _numberOfFrames++;
            _numberOfThrowsInFrame = 0;
            _pinDownInFrame = 0;
        }

        private void CheckIfSpareOrStrike()
        {
            if (_pinDownInFrame == 10)
            {
                if (_numberOfThrowsInFrame == 1)
                {
                    _numberOfBonusPoints += 1;
                }
                else
                {
                    _numberOfBonusPoints += 2;
                    _numberOfStrikes++;
                }
            }
        }

        private void AddBonusPoints(int i)
        {
            if (_numberOfBonusPoints > 0)
            {
                Score += i;
                if (_numberOfStrikes > 1)
                {
                    Score += i;
                    _numberOfStrikes--;
                }
                _numberOfBonusPoints--;
                //if ((_numberOfThrowsInFrame == 1 && !IsLastFrame) || (_numberOfThrowsInFrame == 2 && IsLastFrame))
                //{
                //    _previousFrameIsStrike = false;
                //}
            }
        }
    }
}