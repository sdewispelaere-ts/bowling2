/*
 * This file is owned by TalentSoft.NUnit.Setup NuGet package and should not be modified.
 */

// ReSharper disable CheckNamespace
using System;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

[SuppressMessage("TalentSoft.StyleCopRules.TSRules", "TS0601:ClassShouldBeIncludedInANamespace", Justification = "This is a set up class for the whole assembly and must have no namespace")]
public partial class AssemblyTestInit
{
    /// <summary>
    /// Set the current AppDomain for tests.
    /// </summary>
    /// <remarks>
    /// We just need to set data in order to fix VirtualPathUtility in unit test.
    /// See http://stackoverflow.com/a/25670515/
    /// IT IS MANDATORY FOR THIS METHOD TO RUN BEFORE ANY CALL TO HttpRuntime.
    /// </remarks>
    [OneTimeSetUp]
    public void SetCurrentDomain()
    {
        AppDomain.CurrentDomain.SetData(".appDomain", "*");
        AppDomain.CurrentDomain.SetData(".appPath", AppDomain.CurrentDomain.BaseDirectory);
        AppDomain.CurrentDomain.SetData(".appVPath", "/");
    }
}