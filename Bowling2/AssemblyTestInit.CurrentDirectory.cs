﻿/*
 * This file is owned by TalentSoft.NUnit.Setup NuGet package and should not be modified.
 */

// ReSharper disable CheckNamespace
using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;

using NUnit.Framework;

[SuppressMessage("TalentSoft.StyleCopRules.TSRules", "TS0601:ClassShouldBeIncludedInANamespace", Justification = "This is a set up class for the whole assembly and must have no namespace")]
public partial class AssemblyTestInit
{
    private string OriginalCurrentDirectory { get; set; }

    /// <summary>
    /// Push current domain directory as the current directory to avoid to work in the runner directory.
    /// <para>
    /// It's useful for backward compatibility as it was native with NUnit2 but not with NUnit3.
    /// It also prevents tests to mess up the runner directory in particular when running tests with ReSharper.
    /// </para>
    /// </summary>
    [OneTimeSetUp]
    public void PushCurrentDirectory()
    {
        var currentDirectory = Directory.GetCurrentDirectory();
        var domainDirectory = AppDomain.CurrentDomain.BaseDirectory;

        if (currentDirectory != domainDirectory)
        {
            Directory.SetCurrentDirectory(domainDirectory);
            this.OriginalCurrentDirectory = currentDirectory;
        }
    }

    /// <summary>
    /// Pop current directory before giving hand back to the runner so that it can launch tests for the next assembly.
    /// </summary>
    [OneTimeTearDown]
    public void PopCurrentDirectory()
    {
        if (!string.IsNullOrEmpty(this.OriginalCurrentDirectory))
        {
            Directory.SetCurrentDirectory(this.OriginalCurrentDirectory);
        }
    }
}